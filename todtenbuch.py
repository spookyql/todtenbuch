from prompt_toolkit import prompt
from prompt_toolkit.history import InMemoryHistory
from sys import argv
from importlib import import_module

if len(argv) < 2:
    exit('need to give path as argument')

module_name = argv[-1].split('.', 1)[0]
module = import_module(module_name)
connection = eval(argv[-1], {module.__name__:module})

buffer = ''
history = InMemoryHistory()

while True:
    try:
        buffer = buffer + prompt('> ', history=history)
    except KeyboardInterrupt:
        break
    except EOFError:
        break
        
    if buffer == '.quit':
        break
    
    if ';' in buffer:
        cursor = connection.cursor()

        statements = buffer.split(';')

        read_only = True
        
        for statement in statements:
            if not statement:
                continue
            
            history.append_string(statement + ';')
            try:
                cursor.execute(statement)

                print(f'execute: {statement}')
                head = True
                while row := cursor.fetchone():
                    if head:
                        header = ', '.join(field[0] for field in cursor.description)
                        print(header)
                        print((len(header)) * '-')
                        head = False
                    
                    print(repr(row)[1:-1])
                    
            except Exception as e:
                print(type(e), e)
                raise

        buffer = statements[-1]
    
    connection.commit()
